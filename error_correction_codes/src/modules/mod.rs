

pub mod matrix_operations{

    use peroxide::{prelude::py_matrix};
    use bit_vec::BitVec;

    pub fn count_dimension(char_vec : &Vec<char>) -> (i32, i32){

        let mut i = 0;
        let mut j = 0;
        let mut temp = false;
        let mut start_counting_number_of_columns = true;
    
        for e in char_vec{
            if *e=='['{
                i+=1;
            }
            else if *e==']'{
                start_counting_number_of_columns=false;
                temp=false;
            }else if *e==','{
                temp=false;
            }
            else if (*e!='['||*e!=']')&&!temp&&start_counting_number_of_columns{
                j+=1;
                temp = true;
            }
        }
        i-=1;
    
        (i,j)
    }
    
    
    pub fn get_vector_of_values(mut matrix_from_file : String) -> Vec<i32>{
        let mut temp_char = String::new();
    
        matrix_from_file.retain(|c| (c!='['));
        matrix_from_file.retain(|c| (c!=']'));
        matrix_from_file.push('!');
    
        let matrix_from_file_vec : Vec<char>= matrix_from_file.chars().collect();
        let mut values_vec : Vec<i32> = Vec::new();
        
        for c in matrix_from_file_vec{
            if c=='!'{
                values_vec.push(temp_char.parse().unwrap());
                temp_char.clear();
            }else if c!=','{
                temp_char.push(c);
            }else{
                values_vec.push(temp_char.parse().unwrap());
                temp_char.clear();
            }
        }
        values_vec
    }
    
    
    /*
    pub fn get_matrix_from_file(path : &str) -> (String, String){
        let mut content = std::fs::read_to_string("./src/test.txt")
            .expect("A problem has occured while reading the file, please retry later.");
        content.retain(|c| c !=' ');
    
        let mut matrix_from_file = String::new();
    
        for line in content.lines(){
            matrix_from_file+=line;
        }
        (matrix_from_file, content)
    }
 
    */
    pub fn get_matrix_from_file(path : &str) -> peroxide::structure::matrix::Matrix{
        
        let mut content = std::fs::read_to_string(path)
            .expect("A problem has occured while reading the file, please retry later.");
        content.retain(|c| c !=' ');
    
        let mut matrix_from_file = String::new();
    
        for line in content.lines(){
            matrix_from_file+=line;
        }

        let values_vec : Vec<i32> = get_vector_of_values(matrix_from_file);

        let (i_,j_) = count_dimension(&content.chars().collect());
        let mut vec_res : Vec<Vec<u32>> = Vec::new();
        let mut count : u32 = 0;

        for _i in 0..i_{
            let mut vec_temp : Vec<u32> = Vec::new();
            for _j in 0..j_{
                let test = unsafe{*values_vec.get_unchecked(count as usize)};
                vec_temp.push(test as u32);
                count+=1;
            }
            vec_res.push(vec_temp);
        }

        let matrix_result = py_matrix(vec_res);

        matrix_result
    }
    
    
    pub fn get_systematic_matrix_from_bitvec(matrix : &mut Vec<BitVec>) -> Vec<Vec<f32>>{
        
        let mut A_f : Vec<Vec<f32>> = convert_to_f32_v2(&matrix);
        gauss_jordan_elimination::gauss_jordan_elimination(&mut A_f);
        A_f
    
    }
    
    pub fn get_systematic_matrix_from_bitvec_over_F2(matrix : &mut Vec<BitVec>) -> Vec<Vec<f32>>{
        
        let mut A_f : Vec<Vec<f32>> = convert_to_f32_v2(&matrix);
        let mut A_i : Vec<Vec<f32>> = Vec::new();
    
        gauss_jordan_elimination::gauss_jordan_elimination(&mut A_f);
    
        for e in &A_f{
            let mut temp : Vec<f32> = Vec::new();
            for d in &*e{
                let a = d.abs();
                temp.push(a as f32);
            }
            A_i.push(temp.clone());
            temp.clear();
        }
    
        A_i
    
    }
    
    pub fn convert_to_u8(mut b: &Vec<BitVec>) -> Vec<Vec<u8>> {
        let mut result : Vec<Vec<u8>> = Vec::new();
    
        for e in b{
            let temp : Vec<u8> = e.to_bytes();
            result.push(temp);
        }
        
        result
    }
    
    pub fn convert_to_f32_with_the_whole_u8(mut b: &Vec<BitVec>) -> Vec<Vec<f32>> {
        let mut result : Vec<Vec<f32>> = Vec::new();
        let mut vec_temp : Vec<f32> = Vec::new();
    
        for e in b{
            let byte = e.to_bytes();
    
            let mut s : String = String::new();
            for i in 0..byte.len(){
                let temp;
                if let Some(n) = byte.get(i){
                    temp = n;
                }else{
                    panic!("An error has occured");
                }
                s += &String::from(format!("{:b}", temp));
            }
    
            for e in s.chars(){
                let value;
                if let Some(n) = e.to_digit(10){
                    value = n;
                }else{
                    panic!("An error has occured");
                }
                
                vec_temp.push(if value == 0u32 {0.0} else {1.0});
            }
            result.push(vec_temp.clone());
            vec_temp.clear();
        }
        
        result
    }
    
    pub fn convert_to_f32_v2(mut b: &Vec<BitVec>) -> Vec<Vec<f32>> {
        let mut result : Vec<Vec<f32>> = Vec::new();
        let mut vec_temp : Vec<f32> = Vec::new();
    
        for e in b{
            let byte = e.to_bytes();
    
            let mut s : String = String::new();
            for i in 0..byte.len(){
                let temp;
                if let Some(n) = byte.get(i){
                    temp = n;
                }else{
                    panic!("An error has occured");
                }
                s += &String::from(format!("{:b}", temp));
            }
    
            for (i,p) in s.chars().enumerate(){
                if(i==e.len()){
                    break;
                }
                let value;
                if let Some(n) = p.to_digit(10){
                    value = n;
                }else{
                    panic!("An error has occured");
                }
                
                vec_temp.push(if value == 0u32 {0.0} else {1.0});
            }

            result.push(vec_temp.clone());
            vec_temp.clear();
        }
        
        result
    }
    
    
    pub fn print_bit_vector(bit_vector : &Vec<BitVec>){
    
        print!("[");
        for i in 0..bit_vector.len()-1{
            if let Some(n) = bit_vector.get(i){
                print!("{n:?}, ");
            }else{
                panic!("A problem has occured while trying to get BitVec")
            }
        }
        if(bit_vector.len()!=0){
            if let Some(n) = bit_vector.get(bit_vector.len()-1){
                print!("{n:?}]");
            }else{
                panic!("A problem has occured while trying to get BitVec")
            }
        }
        println!();
    }
    
    pub fn print_f32_vector(vector : &Vec<Vec<f32>>){
        if vector.len() == 0{
            return
        }
        print!("[");
        for i in 0..vector.len()-1{
            let e = vector.get(i).unwrap();
            if e.len() == 0{
                continue;
            }
            print!("{}", if i==0 {"["} else {" ["});
            for i in 0..e.len()-1{
                let temp = e.get(i).unwrap();
                print!("{temp}, ");
            }
            let temp = e.get(e.len()-1).unwrap();
            print!("{temp}],\n");
        }
        let e = vector.get(vector.len()-1).unwrap();
        print!("{}", if vector.len()==0 {"["} else {" ["});
        for i in 0..e.len()-1{
            let temp = e.get(i).unwrap();
            print!("{temp}, ");
        }
        let temp = e.get(e.len()-1).unwrap();
        print!("{temp}],");
        print!("]\n");
    }

    pub fn convert_matrix_u32_to_vec_bitvec_col_peroxyde(matrix : &mut peroxide::structure::matrix::Matrix) -> Vec<BitVec>{
        let mut result : Vec<BitVec> = Vec::new();
        let nbr_of_rows = matrix.row;
        let nbr_of_columns = matrix.col;

        for i in 0..nbr_of_columns{
            let mut bitvec_temp : BitVec;
            bitvec_temp = BitVec::new(); 
            for j in 0..nbr_of_rows{
                let coef = matrix
                            .row(j as usize).clone();

                let value = coef.get(i as usize).expect("An error has occured");
                bitvec_temp.push(if 1==(*value as u32) {true} else {false});
            }
            result.push(bitvec_temp.clone());
            bitvec_temp.clear();
        }
        result
    }

    pub fn convert_bitvec_to_matrix(bitvec : &mut BitVec) -> peroxide::structure::matrix::Matrix{
        let mut vec_temp : Vec<Vec<u32>> = Vec::new();

        let mut vec_tt : Vec<u32> = Vec::new();
        for i in bitvec.iter(){
            vec_tt.push(if i {1} else {0});
        }

        vec_temp.push(vec_tt);

        let result = py_matrix(vec_temp);

        result
    }

    pub fn and_bitvec(bitvec_a : &BitVec, bitvec_b : &BitVec) -> BitVec{
        let mut result : BitVec = BitVec::new();
        let len_a = bitvec_a.len();
        let len_b = bitvec_a.len();

        if len_a != len_b{
            panic!("The two bitvec have not the same length");
        }
        for i in 0..len_a{
            result.push( bitvec_a.get(i).unwrap()&bitvec_b.get(i).unwrap() );
        }
        result
    }

}

pub mod error_correction_codes_functions{

    use ark_poly::multivariate::Term;
    use ark_poly::MVPolynomial;

    pub fn get_dual_code_from_generator_matrix_(matrix_peroxide : &mut peroxide::structure::matrix::Matrix, p : u32) -> HashSet<BitVec>{
        get_code_from_generator_matrix(matrix_peroxide, p)
    }

    pub fn describe_code_from_path(path : &str, q : u32, p : u32){
        
        let mut matrix = get_matrix_from_file(path);

        let mut parity_matrix = get_parity_check_matrix(&mut matrix, p);
    
        let (n, k, dmin) = get_parameters_from_matrix(&mut matrix, p);

        let (n_T, k_T, dmin_T) = get_parameters_from_matrix(&mut parity_matrix, p);

        let code = get_code_from_generator_matrix(&mut matrix, p);
        
        let dual_code = get_dual_code_from_generator_matrix_(&mut matrix, p);
        
        let code_weight_distribution = get_weight_distribution(&code);

        let dual_code_weight_distribution = get_weight_distribution(&dual_code);
    
        let zeta_polynomial = get_zeta_polynomial_from_generator_matrix(&mut matrix, p, q);

        print!("C = ");
        print_code(&code);
        println!();
        println!("(n, k, dmin) = ({}, {}, {})\n", n, k, dmin);
        print!("C_dual = ");
        print_code(&dual_code);
        println!();
        println!("(n_T, k_T, dmin_T) = ({}, {}, {})\n", n_T, k_T, dmin_T);
        println!("Weight distribution of C :");
        for (key, value) in code_weight_distribution{
            println!("weight = {:?}, words = {:?}", key, value);
        }
        println!();
        println!("Weight distribution of the dual code of C :");
        for (key, value) in dual_code_weight_distribution{
            println!("weight = {:?}, word(s) = {:?}", key, value);
        }
        println!();
        print!("zeta polynomial of the code : ");
        zeta_polynomial.print();
        println!();
    
    }

    pub fn print_code(code : &HashSet<BitVec>){
        let mut str = String::from("{");
        code.iter().for_each(|e| {
            let str_ = format!("{e:?}, ");
            str.push_str(&str_);
        });

        let mut final_string = String::from(&(str.as_str())[0..(str.as_str()).len()-2]);
        final_string.push_str("}");
        println!("{}", final_string);
    }

    pub fn describe_code_from_matrix(matrix : &mut peroxide::structure::matrix::Matrix, p : u32, q : u32){

        let mut parity_matrix = get_parity_check_matrix(matrix, p);
    
        let (n, k, dmin) = get_parameters_from_matrix(matrix, p);

        let (n_T, k_T, dmin_T) = get_parameters_from_matrix(&mut parity_matrix, p);

        let code = get_code_from_generator_matrix(matrix, p);
        
        let dual_code = get_dual_code_from_generator_matrix_(matrix, p);
        
        let code_weight_distribution = get_weight_distribution(&code);

        let dual_code_weight_distribution = get_weight_distribution(&dual_code);
    
        let zeta_polynomial = get_zeta_polynomial_from_generator_matrix(matrix, p, q);

        print!("C = ");
        print_code(&code);
        println!();
        println!("(n, k, dmin) = ({}, {}, {})\n", n, k, dmin);
        print!("C_dual = ");
        print_code(&dual_code);
        println!();
        println!("(n_T, k_T, dmin_T) = ({}, {}, {})\n", n_T, k_T, dmin_T);
        println!("Weight distribution of C :");
        for (key, value) in code_weight_distribution{
            println!("weight = {:?}, words = {:?}", key, value);
        }
        println!();
        println!("Weight distribution of the dual code of C :");
        for (key, value) in dual_code_weight_distribution{
            println!("weight = {:?}, word(s) = {:?}", key, value);
        }
        println!();
        print!("zeta polynomial of the code : ");
        zeta_polynomial.print();
        println!();

    }

    pub fn get_parity_check_matrix(generator_matrix : &mut peroxide::structure::matrix::Matrix, p : u32)
    -> peroxide::structure::matrix::Matrix{

        let (n, k, dmin): (u32, u32, u32) = get_parameters_from_matrix(generator_matrix, p);


        let parity_check_matrix = get_systematic_matrix(generator_matrix);

        let mut vec_identity : Vec<Vec<u32>> = Vec::new();
        let mut vec_P : Vec<Vec<u32>> = Vec::new();
        
        for i in 0..(n-k){
            let mut vec_temp : Vec<u32> = Vec::new();
            for j in 0..k{
                if j==i{
                    vec_temp.push(1);
                }else{
                    vec_temp.push(0);
                }
            }
            vec_identity.push(vec_temp);
        }
        for i in 0..parity_check_matrix.row{
            let mut vec_row : Vec<u32> = Vec::new();
            for j in k..n{
                let coef = parity_check_matrix
                            .row(i as usize).clone();

                let value = coef.get(j as usize).expect("An error has occured");

                vec_row.push(*value as u32);
            }
            vec_P.push(vec_row);
            
        }

        let identity_matrix = py_matrix(vec_identity);

        let real_identity = eye((n-k) as usize);

        let P_matrix = py_matrix(vec_P);

        let parity_check_matrix = cbind(-P_matrix.transpose(), real_identity);

        let result : peroxide::structure::matrix::Matrix = get_matrice_whith_coefficient_modulo_p(&parity_check_matrix, p);

        result

    }

    pub fn get_matrice_whith_coefficient_modulo_p(matrix : &peroxide::structure::matrix::Matrix , p : u32)
    -> peroxide::structure::matrix::Matrix{

        let mut result_vect : Vec<Vec<u32>> = Vec::new();

        for i in 0..matrix.row{
            let mut vec_temp : Vec<u32> = Vec::new();
            for j in 0..matrix.col{

                let coef = matrix
                            .row(i as usize).clone();

                let value = coef.get(j as usize).expect("An error has occured");

                vec_temp.push((*value as i32).rem_euclid(p as i32) as u32);
            }
            result_vect.push(vec_temp);
        }

        let result = py_matrix(result_vect);

        result

    }

    pub fn get_systematic_matrix(generator_matrix : &mut peroxide::structure::matrix::Matrix) -> 
    peroxide::structure::matrix::Matrix{
        generator_matrix.rref()
    }

    fn get_D_duursma(c: Vec<f32>) -> Polynomial{
        let mut vec_poly : Vec<f64> = Vec::new();
        for i in 0..(c.len() as u32){
            vec_poly.push(*c.get(((c.len() as u32)-(i + 1)) as usize).expect("") as f64);
        }
        let poly = poly(vec_poly);
        poly
    }

    fn get_c_duursma(matrix_peroxide: &mut peroxide::structure::matrix::Matrix, p: u32, q: u32)
    -> Vec<f32>{

        let code = get_code_from_generator_matrix(matrix_peroxide, p);

        let weight_distribution : HashMap<u32, HashSet<BitVec>>
                = get_weight_distribution(&code);
        
        let dual_code = get_dual_code_from_generator_matrix_(matrix_peroxide, p);

        let (n, k, dmin) = get_parameters_from_code(&code);
        let (n_T, k_T, dmin_T) = get_parameters_from_code(&dual_code);
         
        let g = n+1-k-dmin;
        let g_T = n_T+1-k_T-dmin_T;

        let r = g + g_T;

        let mut c : Vec<f32> = Vec::new();

        let (n, k, dmin) = get_parameters_from_code(&code);

        for i in 0..((r-2)+1){
            
            let bin_iter_0 : BinomialIter = BinomialIter::new(n, dmin+i);
            
            let temp_0 = ((1/(q-1)) as f32)*(1f32/((bin_iter_0.binom() as f32)));
            
            let mut temp_1 : f32 = 0f32;
            for w in dmin..((dmin+i) + 1){
                let bin_iter_i : BinomialIter = BinomialIter::new(n-w, n-dmin-i);
                temp_1 += (bin_iter_i.binom() as f32)*
                    (weight_distribution.get(&w).expect("").len() as f32);
                
            }
            
            c.push( (temp_0*temp_1) as f32);
        }

        c

    }

    pub fn get_zeta_function(matrix_peroxide: &mut peroxide::structure::matrix::Matrix, p: u32, q: u32){
        let zeta_polynomial = get_zeta_polynomial_from_generator_matrix(matrix_peroxide, p, q);
        todo!()
    }

    pub fn get_zeta_polynomial_from_generator_matrix(matrix_peroxide: &mut peroxide::structure::matrix::Matrix, p: u32, q: u32)
     -> Polynomial{

        let code = get_code_from_generator_matrix(matrix_peroxide, p);

        let (n, k, dmin) = get_parameters_from_code(&code);
        let g : u32 = n + 1 - k - dmin;

        let mut c = get_c_duursma(matrix_peroxide, p, q);
        let mut poly_D : Polynomial = get_D_duursma(c);

        let mut vec_a = vec![-1 as f64, 1 as f64];
        let mut poly_a = Polynomial::new(vec_a);

        let mut vec_b = vec![-i64::from(q) as f64, 1 as f64];
        let mut poly_b = Polynomial::new(vec_b);

        let mut poly_c = poly_a.mul(poly_b);

        let mut poly_d = poly_c.mul(poly_D);

        let mut vec_t_g : Vec<f64> = Vec::new();

        for i in 0..g+1{
            if i!=0 {
                vec_t_g.push(0 as f64);
            }else{
                vec_t_g.push(1 as f64);
            }
        }

        let mut poly = poly_d.add(Polynomial::new(vec_t_g));

        poly

    }

    pub fn get_weight_enumerator(code : &HashSet<BitVec>) ->
    ark_poly::multivariate::SparsePolynomial<ark_ff::Fp384<ark_bls12_381::FqParameters>, 
        ark_poly::multivariate::SparseTerm> {

        let (n, k, dmin) : (u32, u32, u32) = get_parameters_from_code(code);
        let weight_distribution = get_weight_distribution(code);

        let mut poly_vec : 
            Vec<(ark_ff::Fp384<ark_bls12_381::FqParameters>, SparseTerm)> = Vec::new();

        poly_vec.push((Fq::from(1),  SparseTerm::new(vec![(0, n as usize)])));
        for i in dmin..n+1{
            let mut cardinal_i = 0;
            if let Some(n) = weight_distribution.get(&i) {
                cardinal_i = n.len()
            }else{
                continue;
            }
            poly_vec.push((Fq::from(cardinal_i as u32), 
                SparseTerm::new(vec![(0,((n-i) as usize)),(1,(i as usize))])));
        }

        let poly = SparsePolynomial::from_coefficients_vec(2, poly_vec);
        
        poly
    }

    use ark_bls12_381::Fq;
    use ark_poly::multivariate::SparseTerm;
    use ark_poly::multivariate::SparsePolynomial;
    use binomial_iter::BinomialIter;
    use peroxide::fuga::LinearAlgebra;
    use peroxide::prelude::MATLAB;
    use peroxide::prelude::Polynomial;
    use peroxide::prelude::Printable;
    use peroxide::prelude::cbind;
    use peroxide::prelude::eye;
    use peroxide::prelude::poly;
    use peroxide::prelude::py_matrix;
    use peroxide::structure;
    use peroxide::structure::matrix;
    use twenty_first::shared_math::traits::New;
    use crate::modules::matrix_operations::*;
    use bit_vec::*;
    use rulinalg::matrix::*;
    use std::collections::*;
    use std::ops::Add;
    use std::ops::Mul;
    use std::vec;

    pub fn get_dmin_from_code(code : &HashSet<BitVec>) -> u32{

        let mut dmin : u32 = code.len() as u32;
        if code.len()==0{
            panic!("the code is empty");
        }

        for e in code.iter(){
            for d in code.iter(){
                if e==d {
                    continue;
                }else{
                    let dist = get_distance(e, d);
                    if dist <= dmin{
                        dmin = dist;
                    }
                }
            }
        }

        dmin
    }

    pub fn get_distance(word_a : &BitVec, word_b : &BitVec) -> u32{

        let mut count : u32 = 0;

        if word_a.len()!=word_b.len() {
            panic!("the two words are different");
        }

        for i in 0..word_a.len(){
            if word_a.get(i).expect("")!=word_b.get(i).expect(""){
                count+=1;
            }
        }

        count
    }

    pub fn get_parameters_from_matrix(generator_matrix : &mut peroxide::structure::matrix::Matrix, p : u32) 
        -> (u32, u32, u32){
  
        let mut n : u32 = 0;
        let mut k : u32 = 0;
        let mut dmin : u32 = 0;
        let mut code : HashSet<BitVec> = HashSet::new();

        k = generator_matrix.row as u32;
        n = generator_matrix.col as u32;
        code = get_code_from_generator_matrix(generator_matrix, p);
        dmin = get_dmin_from_code(&code);

        (n, k, dmin)
    }

    pub fn get_parameters_from_code(code : &HashSet<BitVec>) -> (u32, u32, u32){
        let mut n : u32 = 0;
        let mut k : u32 = 0;
        let mut dmin : u32 = 0;

        k = (code.len() as f64).log2() as u32;
        for e in code.iter(){
            n = e.len() as u32;
            break;
        }

        dmin = get_dmin_from_code(code);

        (n, k, dmin)
    }

    pub fn get_weight_distribution(code : &HashSet<BitVec>) -> HashMap<u32, HashSet<BitVec>>{
        let mut weight_distribution : HashMap<u32, HashSet<BitVec>> = HashMap::new();
        if code.len()==0{
            panic!("the code is empty");
        }
        let mut len : u32 = 0;

        //à corriger
        for e in code{
            len = e.len() as u32;
            break;
        }

        for i in 0..len+1{
            let mut set : HashSet<BitVec> = HashSet::new();
            for e in code{
                if get_weight_of_word_over_F2(e.clone())==i{
                    set.insert(e.clone());
                }
            }
            weight_distribution.insert(i, set);
        }
        weight_distribution
    }

    pub fn get_weight_of_word_over_F2(word : BitVec) -> u32{
        let mut result : u32 = 0;
        for e in word{
            if e==true{
                result+=1;
            }
        }
        result
    }

    pub fn get_code_from_generator_matrix(generator_matrix : &mut peroxide::structure::matrix::Matrix, p:u32) -> HashSet<BitVec>{

        let words_vec : Vec<u32> = Vec::new();
        let mut code : HashSet<BitVec> = HashSet::new();
        let k : u32 = generator_matrix.row as u32;
        let n : u32 = generator_matrix.col as u32;
        let generator_vec_of_bitvec : Vec<BitVec> = convert_matrix_u32_to_vec_bitvec_col_peroxyde(generator_matrix);
        let base: u32 = 2;

        let max = base.pow(k);

        for i in 0..max{

            let str = format!("{:b}", i);
            let vec_chars : Vec<char> = str.chars().collect();
            let mut vector_temp : Vec<u32> = Vec::new();

            let len_of_str = str.len() as u32;
            for o in 0..(k-len_of_str){
                vector_temp.push(0);
            }

            for e in str.chars(){
                if let Some(n) = e.to_digit(10){
                    vector_temp.push(n);
                }else{
                    panic!("An error has occured");
                }
            }

            let mut d : BitVec = BitVec::new();
            for e in vector_temp{
                d.push(if e==1 {true} else {false});
            }

            let mut to_add: BitVec = BitVec::new();
            for l in 0..n{
                let result = unsafe{&generator_vec_of_bitvec.get_unchecked(l as usize)};
                
                let mut sum : u8 = 0;
                for y in 0..k{
                    let left = if d.get(y as usize).expect("") {1} else {0};
                    let right = if result.get(y as usize).expect("") {1} else {0};
                    sum+=left*right;
                }
                sum = (sum).rem_euclid(p as u8);
                to_add.push(if sum==1 {true} else {false});
            }
            code.insert(to_add.clone());
            to_add.clear();
        }
        code
    }

}