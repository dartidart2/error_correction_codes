//#![windows_subsystem = "windows"]

use std::env;
use terminal_menu::{mut_menu, button, activate, wait_for_exit, run, menu, scroll, list, string, numeric, submenu, back_button, label, TerminalMenuItem};

mod modules;

fn main() {
    //modules::error_correction_codes_functions::describe_code_from_path("./src/test.txt", 2, 2);

    let args: Vec<String> = env::args().collect();

    if args.len()!=3{
        panic!("the arguments are no correct");
    }

    let query = &args[1];
    let file_path = &args[2];

    //println!("Searching for {}", query);
    //println!("In file {}", file_path);

    if query!="file"{
        println!("the command \"{}\" is not recognized, please retry", query);
    }

    let (p, q) = (2,2);

    let mut matrix = modules::matrix_operations::get_matrix_from_file(file_path);

    let mut parity_matrix = modules::error_correction_codes_functions::get_parity_check_matrix(&mut matrix, p);
    
    let (n, k, dmin) = modules::error_correction_codes_functions::get_parameters_from_matrix(&mut matrix, p);

    let (n_T, k_T, dmin_T) = modules::error_correction_codes_functions::get_parameters_from_matrix(&mut parity_matrix, p);

    let code = modules::error_correction_codes_functions::get_code_from_generator_matrix(&mut matrix, p);
        
    let dual_code = modules::error_correction_codes_functions::get_dual_code_from_generator_matrix_(&mut matrix, p);
        
    let code_weight_distribution = modules::error_correction_codes_functions::get_weight_distribution(&code);

    let dual_code_weight_distribution = modules::error_correction_codes_functions::get_weight_distribution(&dual_code);
    
    let zeta_polynomial = modules::error_correction_codes_functions::get_zeta_polynomial_from_generator_matrix(&mut matrix, p, q);

    //definitions of the vectors

    let mut parameters_code : Vec<TerminalMenuItem> = Vec::new();
    let mut parameters_dual_code : Vec<TerminalMenuItem> = Vec::new();
    let mut show_code : Vec<TerminalMenuItem> = Vec::new();
    let mut show_dual_code : Vec<TerminalMenuItem> = Vec::new();

    //
    let mut zeta_polynomial_str : Vec<TerminalMenuItem> = vec![back_button(zeta_polynomial.to_string())];
    zeta_polynomial_str.push(back_button("Back"));

    //
    
    let mut str_n : &str = &format!("n = {}", n);
    let mut str_k : &str = &format!("k = {}", k);
    let mut str_dmin : &str = &format!("dmin = {}", dmin);

    parameters_code.push(label(str_n));
    parameters_code.push(label(str_k));
    parameters_code.push(label(str_dmin));
    parameters_code.push(back_button("Back"));

    //

    let mut str_n_T : &str = &format!("n_T = {}", n_T);
    let mut str_k_T : &str = &format!("k_T = {}", k_T);
    let mut str_dmin_T : &str = &format!("dmin_T = {}", dmin_T);

    parameters_dual_code.push(label(str_n_T));
    parameters_dual_code.push(label(str_k_T));
    parameters_dual_code.push(label(str_dmin_T));
    parameters_dual_code.push(back_button("Back"));

    //

    code.iter().for_each(|e| {
        let str_ = format!("{e:?}");
        show_code.push(back_button(str_));
    });
    show_code.push(back_button("Back"));

    //println!("len of show_code : {}", show_code.len());

    //

    dual_code.iter().for_each(|e| {
        let str_ = format!("{e:?}");
        show_dual_code.push(back_button(str_));
    });
    show_dual_code.push(back_button("Back"));

    //

    let label_file = label(file_path);

    let menu = menu(vec![
        submenu("get parameters of the code", parameters_code),
        submenu("get parameters of the dual code", parameters_dual_code),
        submenu("show the code", show_code),
        submenu("show the dual code", show_dual_code),
        submenu("show zeta polynomial", zeta_polynomial_str),
        back_button("Exit")
    ]);
    run(&menu);

}
