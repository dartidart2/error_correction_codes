rust     NE#rustc 1.64.0 (a55dd71d5 2022-09-19)�core���������� -15d7d0533432428b�compiler_builtins�ձ��Ú��� -cfe8447efe28cb1e�rustc_std_workspace_core��������& -015e9ae63a74e3a1�alloc�����ˮ�$ -32d7ee0be9e46c85�N      core�  compiler_builtins�  alloc�          	 
Allocation� D� Native� 
 Foreign� 
 0�  
ForeignVec� D� T� data� 
allocation�   D� T� from_foreign� get_vec�  D� T� drop�  #D� #T� #Target� #deref�  (D� (T� (fmt�  ,D� ,T� ,from� '_� "'_� ''_� +'_� +'_�+'_�,  # ( ����P  �     �   Foreign�  D�    �     �         �           Native�     Foreign� 0�        ���������   �    �   �      �      �   doc�      �This library offers [`ForeignVec`], a zero-cost abstraction to store either [`Vec<T>`]\nor an immutable region aligned with `T` allocated by an external allocator.\n\nThe primary use-case of this library is when you have an in-memory representation\nin both Rust and other languages and you have a specification to share\n(immutable) vectors across language boundaries at zero cost, via FFI.\n\nIn this scenario, you may want to still offer all the benefits of Rust\'s `Vec`\nwhen it comes to mutable access, while providing a read-only access when the\ndata came from a foreign interface. In other words, given\n\n* an in-memory format\n* an FFI specification to share immutable memory regions at zero cost at language\n  boundaries\n\nthen, [`ForeignVec`] offers an interface to\n\n* allow zero-cost immutable access via `core::ops::Deref<T>` to `Vec<T>` or\n  the foreign vector\n* allow access to `&mut Vec<T>` when it is allocated by Rust\n\nThe crucial point here is \"zero-cost immutable access\". The usual idiom\nhere is to have an `enum` with two variants, `Native(Vec<T>)` and another.\nHowever, such enum incurs a significant (`+50%`) cost when deferring the enum\ninto `&[T]`.\n\nThe complete test:\n\n```rust\nuse foreign_vec::ForeignVec;\n\n// say that we have a foreign struct allocated by an external allocator (e.g. C++)\n// owning an immutable memory region\n#[repr(C)]\nstruct Foreign {\n    ptr: *const i32,\n    length: usize,\n    // this is usually created at the FFI boundary; `capacity` is usually \"hidden\" in that\n    // it could contain a C++ `shared_ptr` or something else describing the region\n    // \"on the other side\".\n    capacity: usize,\n}\n\n// whose drop calls an external function that deallocates the region\nimpl Drop for Foreign {\n    fn drop(&mut self) {\n        // mocking an external deallocation\n        unsafe { Vec::from_raw_parts(self.ptr as *mut i32, self.length, self.capacity) };\n    }\n}\n\n// The type that we use on the library uses `foreign_vec`\n// this could be a generic over `T` when the in-memory format supports multiple types.\ntype MyForeignVec = ForeignVec<Foreign, i32>;\n\n#[test]\nfn test_vec() {\n    // we can use it with `Vec`:\n    let expected: &[i32] = &[1, 2];\n\n    // when we have a vector, we can use `.into()`\n    let vec = expected.to_vec();\n    let mut vec: MyForeignVec = vec.into();\n\n    // deref works as expected\n    assert_eq!(&*vec, expected);\n\n    // debug works as expected\n    assert_eq!(format!(\"{:?}\", vec), \"[1, 2]\");\n\n    // you can retrieve a mut vec (since it is allocated by Rust)\n    assert_eq!(vec.get_vec(), Some(&mut vec![1, 2]));\n\n    // this calls `Vec::drop`, as usual\n    drop(vec)\n}\n\n// this is just `Vec::into_raw_parts`, which is only available in unstable channels\nfn into_raw_parts<T>(vec: Vec<T>) -> (*mut T, usize, usize) {\n    let r = (vec.as_ptr() as *mut T, vec.len(), vec.capacity());\n    std::mem::forget(vec);\n    r\n}\n\n#[test]\nfn test_foreign() {\n    // on an externally allocated pointer (here from Rust, but a foreign call would do the same)\n    let expected: &[i32] = &[1, 2];\n\n    let a = expected.to_vec();\n    let (ptr, length, capacity) = into_raw_parts(a);\n    // this is usually created at the FFI boundary; `capacity` is usually \"hidden\" in that\n    // it could contain a C++ `shared_ptr` instead.\n    let a = Foreign {\n        ptr,\n        length,\n        capacity,\n    };\n\n    // create a `MyForeignVec` from a foreign that implements `Deref`.\n    let mut vec = unsafe { MyForeignVec::from_foreign(a.ptr, a.length, a) };\n    assert_eq!(&*vec, expected);\n    assert_eq!(vec.get_vec(), None);\n\n    // this calls `Foreign::drop`, which calls the foreign function\n    drop(vec);\n}\n\n```\n�  �This library offers [`ForeignVec`], a zero-cost abstraction to store either [`Vec<T>`]
or an immutable region aligned with `T` allocated by an external allocator.

The primary use-case of this library is when you have an in-memory representation
in both Rust and other languages and you have a specification to share
(immutable) vectors across language boundaries at zero cost, via FFI.

In this scenario, you may want to still offer all the benefits of Rust's `Vec`
when it comes to mutable access, while providing a read-only access when the
data came from a foreign interface. In other words, given

* an in-memory format
* an FFI specification to share immutable memory regions at zero cost at language
  boundaries

then, [`ForeignVec`] offers an interface to

* allow zero-cost immutable access via `core::ops::Deref<T>` to `Vec<T>` or
  the foreign vector
* allow access to `&mut Vec<T>` when it is allocated by Rust

The crucial point here is "zero-cost immutable access". The usual idiom
here is to have an `enum` with two variants, `Native(Vec<T>)` and another.
However, such enum incurs a significant (`+50%`) cost when deferring the enum
into `&[T]`.

The complete test:

```rust
use foreign_vec::ForeignVec;

// say that we have a foreign struct allocated by an external allocator (e.g. C++)
// owning an immutable memory region
#[repr(C)]
struct Foreign {
    ptr: *const i32,
    length: usize,
    // this is usually created at the FFI boundary; `capacity` is usually "hidden" in that
    // it could contain a C++ `shared_ptr` or something else describing the region
    // "on the other side".
    capacity: usize,
}

// whose drop calls an external function that deallocates the region
impl Drop for Foreign {
    fn drop(&mut self) {
        // mocking an external deallocation
        unsafe { Vec::from_raw_parts(self.ptr as *mut i32, self.length, self.capacity) };
    }
}

// The type that we use on the library uses `foreign_vec`
// this could be a generic over `T` when the in-memory format supports multiple types.
type MyForeignVec = ForeignVec<Foreign, i32>;

#[test]
fn test_vec() {
    // we can use it with `Vec`:
    let expected: &[i32] = &[1, 2];

    // when we have a vector, we can use `.into()`
    let vec = expected.to_vec();
    let mut vec: MyForeignVec = vec.into();

    // deref works as expected
    assert_eq!(&*vec, expected);

    // debug works as expected
    assert_eq!(format!("{:?}", vec), "[1, 2]");

    // you can retrieve a mut vec (since it is allocated by Rust)
    assert_eq!(vec.get_vec(), Some(&mut vec![1, 2]));

    // this calls `Vec::drop`, as usual
    drop(vec)
}

// this is just `Vec::into_raw_parts`, which is only available in unstable channels
fn into_raw_parts<T>(vec: Vec<T>) -> (*mut T, usize, usize) {
    let r = (vec.as_ptr() as *mut T, vec.len(), vec.capacity());
    std::mem::forget(vec);
    r
}

#[test]
fn test_foreign() {
    // on an externally allocated pointer (here from Rust, but a foreign call would do the same)
    let expected: &[i32] = &[1, 2];

    let a = expected.to_vec();
    let (ptr, length, capacity) = into_raw_parts(a);
    // this is usually created at the FFI boundary; `capacity` is usually "hidden" in that
    // it could contain a C++ `shared_ptr` instead.
    let a = Foreign {
        ptr,
        length,
        capacity,
    };

    // create a `MyForeignVec` from a foreign that implements `Deref`.
    let mut vec = unsafe { MyForeignVec::from_foreign(a.ptr, a.length, a) };
    assert_eq!(&*vec, expected);
    assert_eq!(vec.get_vec(), None);

    // this calls `Foreign::drop`, which calls the foreign function
    drop(vec);
}

```
�  	     !   %deny�  %    )  6   missing_docs�   *     "   <no_std�  <       9
    prelude_import�            	macro_use�
         	macro_use�          E    R  ^    i    {    �    �    �    � $ Mode of deallocating memory regions�   �'    �
     D�             ��    �  �    �          �  Native allocation�   �    �           �  Native allocation�   �    �                        �    �           �    �                       �    �           � = A continuous memory region that may be allocated externally.�   �@  �   � 7 In the most common case, this is created from [`Vec`].�   �: F However, this region may also be allocated by a foreign allocator `D`�   �I  and behave as `&[T]`.�   �    �
     D�      T�            �D  �  �T�    �  �    �          �    �          � N An implementation using an `enum` of a `Vec` or a foreign pointer is not used�   �Q J because `deref` is at least 50% more expensive than the deref of a `Vec`.�   �M    �           �  the region was allocated�   �    �
           �       D�      T�            �D  � �I  �  �    �          �    �          �	J / Takes ownership of an allocated memory region.�   �2 	 # Panics�   � 8 This function panics if and only if pointer is not null�   �; 	 # Safety�   � A This function is safe if and only if `ptr` is valid for `length`�   �D  # Implementation�   � ? This function leaks if and only if `owner` does not deallocate�   �B - the region `[ptr, ptr+length[` when dropped.�   �	0    �
                     �0 J Returns a `Some` mutable reference of [`Vec<T>`] iff this was initialized�   �M ( from a [`Vec<T>`] and `None` otherwise.�   �+    �                   �    �$       D�       T� !        !    �D  � �I  �  �    �          �    �          �    �                  �    �0       D� $     T� %     $  %    �D  � �I  �  �    �          �    �          �    �  #     #   �    �             #     � #   �B       D� )     T� *     )  *    �D  � �I  �  �P�I    �  �    �          �    �          �D    �              (     � (   �,       D� -     T� .     -  .    �D  � �I  �  �    �          �    �          �    �              ,     ,   �     �     �     �     �     �   core�      compiler_builtins�      alloc�  R       E ManuallyDrop�  i ��    ^ DerefMut�  � �    { Vec�  � �<    � �    ����������  �  �   � ��     � ��      ��Ø����@      
ForeignVec�  data�   
allocation�       ��Ø����@��I���� ManuallyDrop� ��value�� �   ֊������p�<�< Vec� �<buf��6�<len��6     ڔ��޲�	�I�?�?�?Global�       �������N ��Xptr�  �
length�  �
owner�  �
  ��I 
�I ��X   self�  � ��I 0'_�   0'_��X ������None�   ����Some���0�       ��եؕ��Y   0'_��Z   "�X��Xself�  � "��I 1'_�   1'_��X    &'�X��X	�Iself�  � '��I 2'_�   2'_��X   2'_��]  +�X�P�Xself�  �f�  � +��I 3'_� 4'_� 5'_�   3'_��X  4'_��P�P 	Formatter� �Pflags��M�Pfill��M�Palign��M�Pwidth��M�P	precision��M�Pbuf��M     ���Ձ����   5'_� ������Ok� ��0�   ����Err���0�       ����ް�ͳ�\������Error�       �ۃӆ�ܚ�   /�X��X�Zdata�  �  /��I �Z�X  �+                                                      �+  �+  �+  �+  �+  �+  �+  �+  �+  �+  F,  �,  �,  �,  �,  -  ?-  �-  �-  �-  �-  �-  �-  �-  .  .  S.  Q.  R.  q.  �/  �/  �/  �/  v     !     4!     T!     v!      �!                      �!                      �!                      �!     "      #"     Z"     �"      �"      �"      �"     L$      `$      t$     ;%     y%      �%      �%      �%     �'     A(      �(      �(      �(      �(      )      ))      =)      V)      �)      �)      �)      �)      &*      f*      z*      �*      �*      �*      �*      �*      �*      �*      �+                                                                                                             �+             �+              �+                     �+                                     �,                                     �-                              .                                     T.                             �/           #"
	!  /!              �!          �!          �!          �!      F"  �"  �"  �"  �"  $          '%  e%  }%          {'  ((  E(          �(  �(          D)  i)  �)          *  **          �*  q  
!  2!  R!  r!  |!          �!          �!          �!  
"  "  U"  �"  �"  �"  �"  G$  [$  o$  6%  t%  �%  �%  �%  �'  <(  |(  �(  �(  �(  )  $)  8)  Q)  })  �)  �)  �)  !*  a*  u*  �*  �*  �*  �*  �*  �*  �*      -!  P!  p!  x!  �!          �!          �!          �!  "  A"  x"  �"  �"  �"   $  N$  b$  "%  `%  {%  �%  �%  j'  (  C(  �(  �(  �(  �(  )  +)  ?)  X)  �)  �)  �)  �)  (*  h*  |*  �*  �*  �*  �*  �*  �*  �*                                                          �!  "  Q"  �"  �"  �"  �"  *$  Y$  m$  2%  p%  �%  �%  �%  �'  8(  j(  �(  �(  �(  �(  ")  6)  M)  y)  �)  �)  �)  *  O*  s*  �*  �*                                                          �!  "  I"  �"  �"  �"  �"  $  S$  g$  *%  h%  �%  �%  �%  ~'  +(  H(  �(  �(  �(  �(  )  0)  E)  l)  �)  �)  �)  *  -*  m*  �*  �*                                                          �+      �+  �+  �+  �+  �+  �+          G,  �,  �,          -  B-  �-          �-  .          .  .  U.          t.  �/          �/                                                                                                                  �!             I"      �"     �"      �"             $                                                             |'     )(                             �(                                     j)                             *                             �*                                                                             �+  �+                                      %-  L-              �-                  (.              ~.              �/                                                                      }"      �"                                      o'  (              �(                  ])              *              �*                                                                                                                              �-              .                  W.              �/                                                                              �                                                                              p                                                                                                                                                                                                                                                                                                                                                                                                �,     4-                             �-                                     .                             ^.                             �/                                                                                                                                                                                                                                                                                        �                             �     �                             6(                             c                                                                                                                                                                          �*     !  +!  N!  n!  v!  �!          �!          �!          �!  "  ?"  v"  �"  �"  �"  �#  L$  `$   %  ^%  y%  �%  �%  h'  (  A(  �(  �(  �(  �(  )  ))  =)  V)  �)  �)  �)  �)  &*  f*  z*  �*  �*  �*  �*  �*  �*  �*                                                          �+                          �+  �   �   �   �         !  %  )  -  1  5  9  =  M  T  `  d  q  u  |  �  �  �  �  �  �  �  �  �  �  �  �  �            *  .  5  <  E  I  P  W  a  i  q  y  �  �  'ק]w4eZ�M3���'ק]w4�&�D��'ק]w4��􆔤g'ק]w4���pJ�'ק]w44����1�'ק]w4Mz��Ě'ק]w4w�~J�d'ק]w4֍��-jѧ'ק]w4��Z�,"H'ק]w4�ʫ}(�c'ק]w4�X�wq���'ק]w4#;��~n��'ק]w4E�Q�� �^'ק]w4�X�J'ק]w4�q5�?�$R'ק]w4�ݍ*��'ק]w4S���D�K'ק]w4{Q��*k'ק]w4�qUcz�'ק]w4�YwR\'ק]w4����X'ק]w4Vl�֢'ק]w45ve>F�6'ק]w4���@P��'ק]w4
j �^'ק]w4<���	$'ק]w4p>1n̮?�'ק]w4��g���K'ק]w4�I+*��'ק]w4ۻ�6���'ק]w4?(���7'ק]w4ad�"�>�h'ק]w4s/3k�,�'ק]w4=�r���6'ק]w4't��n��'ק]w4(�W�˱�'ק]w4���p��'ק]w4��[�>'ק]w4t�5�K��A'ק]w4m����?'ק]w4}��՞`�'ק]w4t�wc�F�'ק]w4\�kqQH��'ק]w4 R�ee�v!'ק]w4��wc��c0'ק]w4ֽ����n�'ק]w4�D�d��'ק]w4���@�Y�'ק]w4�v�J4h��'ק]w4}W �|D9�'ק]w4ސ�Wۺ�'ק]w4&mS��+�'ק]w41�;=��'ק]w4��
�̭f�                                crate�  core�  crate�                                  prelude_import�    'ק]w4�ъ�� include_str�    	  ��   !'ק]w4� ��Ϫ̋�F  G              �F  G  5G  eG  %G  UG  �G  �
ODHT 6       @          ��  'ק]w4�q5�?�$R   'ק]w45ve>F�6   'ק]w4���@P��   'ק]w4#;��~n��   'ק]w4�&�D��   'ק]w4�X�J   'ק]w4��􆔤g   'ק]w4�ݍ*��   'ק]w4Mz��Ě   'ק]w4����X   'ק]w4't��n��"   'ק]w4t�5�K��A&   'ק]w4p>1n̮?�   'ק]w4��wc��c0,   'ק]w4<���	$   'ק]w4���@�Y�/   'ק]w4&mS��+�3   'ק]w4\�kqQH��*   'ק]w41�;=��4   'ק]w4ۻ�6���   'ק]w44����1�   'ק]w4�qUcz�   'ק]w4(�W�˱�#   'ק]w4��
�̭f�5   'ק]w4=�r���6!   'ק]w4ad�"�>�h                       'ק]w4��[�>%   'ק]w4ސ�Wۺ�2   'ק]w4E�Q�� �^   'ק]w4}��՞`�(                       'ק]w4��g���K                                           'ק]w4t�wc�F�)   'ק]w4{Q��*k   'ק]w4m����?'   'ק]w4 R�ee�v!+   'ק]w4�D�d��.                                           'ק]w4
j �^   'ק]w4�I+*��   'ק]w4��Z�,"H   'ק]w4֍��-jѧ   'ק]w4S���D�K   'ק]w4Vl�֢   'ק]w4s/3k�,�    'ק]w4�X�wq���
   'ק]w4���p��$   'ק]w4eZ�M3���    'ק]w4�v�J4h��0                       'ק]w4ֽ����n�-   'ק]w4�YwR\   'ק]w4?(���7                                                               'ק]w4}W �|D9�1   'ק]w4�ʫ}(�c	   'ק]w4���pJ�   'ק]w4w�~J�d   )_B%3uM,w n]I bLzF4�J/
�%��}5N��/O$S%QCX`bK�i���Q1c2)_B%3uM,w n  [C:\Users\ADMIN\.cargo\registry\src\github.com-1ecc6299db9ec823\foreign_vec-0.1.0\src\lib.rs� ����	��5� �S��ԅ�L             �j "(-A;JVR !7@IG5Q!\[aHG,4
R07!,?
' (9$<8
3EK*/$+,
  �Ⱦ���ɳ�ў���ͱ  foreign_vec� x86_64-pc-windows-msvc�-5c0adbcd1750a0a5����窇ڵ������߻4     ��    �    �   ��Z����6��6���� �      � �� �  � �������� �  P PP P  - 0- 00  0    0 �0 �  � ���� �X �X�� �     ��
       