
1) save a text file containing the generating matrix in the following format:

[[1,0,0,0,0,1,1,1],
 [0,1,0,0,1,1,1,0],
 [0,0,1,0,1,1,0,1],
 [0,0,0,1,1,0,1,1]]

even if the matrix has only one row, it is necessary to specify the characters "[" and "]" at the ends. Example:

[[1,0,0,0,0,1,1,1]]

2) execute the program by specifying the "file" parameter followed by the path to the text file

./error_correction_code_program file ./test.txt
